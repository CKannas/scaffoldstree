# ScaffoldsTree

This is a Python implementation of "The Scaffold Tree" [1] based on RDKit [2].

The purpose of this project is to provide:

1. An open source implementation of "The Scaffold Tree" process,
2. A filtering functionality to select scaffold(s) on specific level of the scaffold tree. 

# License

[BSD-3-Clause](https://opensource.org/licenses/BSD-3-Clause) 

# References
[1] [Schuffenhauer, A.; Ertl, P.; Roggo, S.; Wetzel, S.; Koch, M. A.; Waldmann, H. The Scaffold Tree − Visualization of the Scaffold Universe by Hierarchical Scaffold Classification. J. Chem. Inf. Model. 2007, 47 (1), 47–58.](https://pubs.acs.org/doi/abs/10.1021/ci600338x)  
[2] [RDKit](www.rdkit.org)